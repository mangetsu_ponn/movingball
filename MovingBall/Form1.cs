﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MovingBall
{
    public partial class Form1 : Form
    {
        double ballPos_x = 10;
        double ballPos_y = 50;
        double delta_x = 7;
        double delta_y = 14;
        int ballRadius = 10;

        public Form1()
        {
            InitializeComponent();

            timer1.Interval = 33;
            timer1.Elapsed += Update_Elapsed;
            timer1.Enabled = true;
        }

        private void Update_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            ballPos_x += delta_x;
            ballPos_y += delta_y;

            if(ballPos_x - ballRadius < 0 || ballPos_x + ballRadius > this.Bounds.Width)
            {
                delta_x *= -1;
            }

            if(ballPos_y - ballRadius < 0 || ballPos_y + ballRadius > this.Bounds.Height)
            {
                delta_y *= -1;
            }

            Invalidate();
        }

        private void DrawBall_Paint(object sender, PaintEventArgs e)
        {
            SolidBrush solidBrush = new SolidBrush(Color.HotPink);
            float px = (float)this.ballPos_x - this.ballRadius;
            float py = (float)this.ballPos_y - this.ballRadius;
            e.Graphics.FillEllipse(solidBrush, px, py, this.ballRadius * 2, this.ballRadius * 2);
        }

        private void OperateTimer_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Space)
            {
                timer1.Enabled = true;
            }

            if (e.KeyCode == Keys.Enter)
            {
                timer1.Enabled = false;
            }
        }
    }
}
